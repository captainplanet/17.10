#pragma comment(lib,"opencv_core249d.lib")
#pragma comment(lib,"opencv_highgui249d.lib")
#pragma comment(lib,"opencv_imgproc249d.lib")

#include <opencv2\core\core.hpp>
#include <opencv2\highgui\highgui.hpp>
#include <opencv2\imgproc\imgproc.hpp>

#include <stdio.h>

using namespace cv;

int main()
{
	//Mat img = imread("lena.jpg");
	VideoCapture cap(0);
	Mat img;
	while (true)
	{
		cap>>img;
		imshow("lena",img);
		if (waitKey(30)>0) break;
	}
	//system("pause");
	return 0;
}